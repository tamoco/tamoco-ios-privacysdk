//
//  PrivacySDK.h
//  PrivacySDK
//
//  Created by Phil Martin on 25/02/2019.
//  Copyright © 2019 Phil Martin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PrivacySDK.
FOUNDATION_EXPORT double PrivacySDKVersionNumber;

//! Project version string for PrivacySDK.
FOUNDATION_EXPORT const unsigned char PrivacySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PrivacySDK/PublicHeader.h>

#import "Crypto.h"
#include <ifaddrs.h>
