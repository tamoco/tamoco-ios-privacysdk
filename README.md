![logo](https://bitbucket.org/repo/XX5xgGx/images/3404555888-logo.gif.png)

For full details of the SDK and it's capabilities and installation details, continue reading below.

The Tamoco Privacy SDK is compliant with the iAB [TCF Version 1.1]open .
(https://iabeurope.eu/wp-content/uploads/2019/08/IABEurope_TransparencyConsentFramework_v1-1_policy_FINAL.pdf)


## Requirements
The  PrivacySDK supports iOS 9 and above and compiled with XCode 11 and Swift 5.1

### Installation
#### Cocoapod 

To install the Privacy SDK using CocoaPods.

Create your Podfile in your project root directory or run this command in the root directory of your project

```bash
    pod init
```
Your pod file should look something like this

```ruby
    platform :ios, '9.0'

       target 'TamocoTestApp' do
         # use_frameworks! must be left uncommented for the PrivacySDK to function and compile correctly

         use_frameworks!

         # SDK
             pod 'PrivacySDK', '~> 1.0.1'

    end
```

Next run this command

```bash
    pod install
```

This will install the SDK as a new framework in your Pods directory. Please note that at this time a binary framework is included which can be run on iOS devices and the Xcode Simulator.


#### Carthage

To create your Cartfile in your project root directory run this command in the terminal
```bash
    touch Cartfile
```

Then follow the instructions below based on your requirements.

```swift
github "getsentry/sentry-cocoa" "4.3.4"
binary "https://raw.githubusercontent.com/Tamoco/TamocoPackageManager/master/PrivacySDK.json" == 1.0.1
```
Finally in the termianl run this command

```bash
    carthage update
```

The TamocoSDK and the Sentry dependancy will be found in "Builds". From the Build folder drag and drop the Tamoco.framework and Sentry.framework into Link Binary with Libraries.
When added to your App, make sure both the Tamoco.framework and Sentry.framework are both set to "Embed and Sign".
Add a Run Script in build phases add these lines under Input files

```bash
$(SRCROOT)/Carthage/Builds/iOS/PrivacySDK.framework
$(SRCROOT)/Carthage/Builds/iOS/Sentry.framework 
```
and these lines in your Output files
```bash
$(BUILT_PRODUCTS_DIR)/$(FRAMEWORKS_FOLDER_PATH)/PrivacySDK.framework
$(BUILT_PRODUCTS_DIR)/$(FRAMEWORKS_FOLDER_PATH)/Sentry.framework
```

You will also require when using Carthage to strip out the unwanted architechtures when archiving your app to submit to the App Store also add this script to your Build Phase

```
 bash
    APP_PATH="${TARGET_BUILD_DIR}/${WRAPPER_NAME}"

    # This script loops through the frameworks embedded in the application and
    # removes unused architectures.
    find "$APP_PATH" -name '*.framework' -type d | while read -r FRAMEWORK
    do
    FRAMEWORK_EXECUTABLE_NAME=$(defaults read "$FRAMEWORK/Info.plist" CFBundleExecutable)
    FRAMEWORK_EXECUTABLE_PATH="$FRAMEWORK/$FRAMEWORK_EXECUTABLE_NAME"
    echo "Executable is $FRAMEWORK_EXECUTABLE_PATH"

    EXTRACTED_ARCHS=()

    for ARCH in $ARCHS
    do
    echo "Extracting $ARCH from $FRAMEWORK_EXECUTABLE_NAME"
    lipo -extract "$ARCH" "$FRAMEWORK_EXECUTABLE_PATH" -o "$FRAMEWORK_EXECUTABLE_PATH-$ARCH"
    EXTRACTED_ARCHS+=("$FRAMEWORK_EXECUTABLE_PATH-$ARCH")
    done

    echo "Merging extracted architectures: ${ARCHS}"
    lipo -o "$FRAMEWORK_EXECUTABLE_PATH-merged" -create "${EXTRACTED_ARCHS[@]}"
    rm "${EXTRACTED_ARCHS[@]}"

    echo "Replacing original executable with thinned version"
    rm "$FRAMEWORK_EXECUTABLE_PATH"
    mv "$FRAMEWORK_EXECUTABLE_PATH-merged" "$FRAMEWORK_EXECUTABLE_PATH"

    done
```
Also make sure the "Run Script only when installing" is checked.



## Project Configuration
Your app project will require the following configuration settings to function properly with the  PrivacySDK.

### Embedded content
If you are using Obj C project, then you need to go to Build Settings of your target and under "Build Options" set "Embedded Content Contains Swift Code" to YES

## Client Integration
### Update your app delegate
The first step to configure and initialize the PrivacySDK into your app is to modify your main AppDelegate files. While initializing PrivacySDK client, you need to provide proper API Key, API Secret. To change any of this parameters in runtime, just re-instantiate the PrivacySDK client.

In your AppDelegate file, start by including the PrivacySDK module:

```swift
import PrivacySDK
```

Or in case of using Objective C
```objc
#import <PrivacySDK/PrivacySDK-Swift.h>
```

Then, add the following additional PrivacySDK property:
```
	var privacy: PrivacySDK!
```
Next, implement SDK initialisation in method 

```swift
func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool`

	//Set the API Keys
	privacy = PrivacySDK(appID: defaultApiKey, appSecret: defaultApiSecret)
	
```

Next in the ViewController you wish the consent dialog to be presented in, import the PrivacySDK

``` swift
import PrivacySDK
```

For objective c
```obcj
#import <PrivacySDK/PrivacySDK-Swift.h> 
```
To present the consent dialog follow this example, the parameter 'screenName' is the name of the dialog required from the Tamoco backend,  you can have multiple Consent screens linked to one app for different parts of your app. 

See example below on how to present the Consent dialog:

```swift
    var appDele: AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
        AppDelegate.app.privacy.presentPrivacyViewController(screenName: "login") { (response) in
           guard let resp = response else {return} // handle nil, there should always be a response object. Your choice to force unwrap!
           if resp.responseCode >= 200 || resp.responseCode < 300{
               // handle app flow the consent dialog should presented modally automatically is the response is successful
           }
        }
    }
```

The completion block will return a 'ResponseData' Struct which consists these properties.
```swift
    @objc public class ResponseData: NSObject {
        @objc public var responseError: Error?
        @objc public let responseCode: Int
    }
```
### Consent String Updates

To listen to consent string updates use the helper closure, this closure gets called everytime the dialog updates the consent string. There are 2 consent strings a Tamoco Consent string and an iAB Consent string. Both of these strings are stored in the UserDefaults

To use the consent update closure follow this example


```swift
  AppDelegate.app.privacy.recievedIABConsentString = { [weak self] (responseData) in
      // we have successfully generated a iAB consent string
      DispatchQueue.main.async {
          let consentString = UserDefaults.standard.string(forKey: iAB_ConsentString)
          print(consentString ?? "")
         
          let tamocoString = UserDefaults.standard.string(forKey: TamocoConsent_String)
          print(tamocoString ?? "")
      }
  }
```

To access the iAB Consent string follow this example
```swift
  let consentString = UserDefaults.standard.string(forKey: iAB_ConsentString)
```

To access the Tamoco Consent string follow this example
``` swift
  let tamocoString = UserDefaults.standard.string(forKey: TamocoConsent_String)
```

Remember to import the PrivacySDK into the Viewcontroller or Views you require access to 'TamocoConsent_String' or 'iAB_ConsentString'

# Change Log

This can be found [here](CHANGELOG.MD) file.
