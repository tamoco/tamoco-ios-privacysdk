Pod::Spec.new do |s|
  s.name         = "PrivacySDK"
  s.version      = "1.0.1"
  s.summary      = "Tamoco Privacy iOS SDK"
  s.description  = "Tamoco iOS SDK Binary SDK for integrating Consent and privacy, linked to the Tamoco CMP backend"
  s.homepage     = "https://www.tamoco.com"
  s.license      = "Apache License, Version 2.0"
  s.author       = { "Daniel Angel" => "daniel.angel@tamaco.co" }
  s.platform     = :ios, "9.0"
  s.source       = { :git => 'https://bitbucket.org/tamoco/tamoco-ios-privacysdk.git', :tag => "#{s.version}"}
  s.ios.vendored_frameworks = 'PrivacySDK.framework'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '5.0' }
  s.compiler_flags = '-swift-version 4.2'
  s.dependency 'Sentry', '~> 4.3.4'
end